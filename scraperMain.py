'''
********************
     DISCARDED
********************
####################################
# Author: Semeraro Alessandra      #
####################################
'''


from urllib.request import urlopen
from bs4 import BeautifulSoup
import csv
import time
import os

'''
********************
FUNCTIONS DEFINITION
********************
'''

def getOpenParsedPage(url):
    """
    This function opens the page whose URL has been passed as a param; then the page is parsed in a bs4 format to be
    manipulated.
    :param url: a string with the URL of the page to scrape
    :return: a BeautifulSoup object that can be manipulated to access all the parts of the HTML
    """
    # Open page to scrape
    page = urlopen(url)
    # Parse the page in a bs4 format
    soup = BeautifulSoup(page, "html.parser")
    return soup


def checkIfFileExists(file):
    """
    This function checks if a file in the project directory exists or not.
    :param file: a string with the path to the file
    :return: True if the file exists, False if the file doesn't exist
    """
    return os.path.exists(file)


def createNewCSV(csv_name, columns_names):
    """
    This function creates a new CSV file with the columns specified by the param columns_name.
    :param csv_name: a string with the name of the CSV to create
    :param columns_names: an array of strings with the columns to add in the CSV
    """
    with open(csv_name, 'w', newline="") as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(columns_names)


def updateCSVRow(csv_name, columns_values):
    """
    This function updates a CSV file's columns with the values specified by the param columns_values.
    :param csv_name: a string with the name of the CSV to update
    :param columns_values: an array of strings with the columns values to add in the CSV
    """
    with open(csv_name, 'a', newline="") as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(columns_values)

'''
********************
   START OF CODE
********************
'''

# Main URL for the site (we will use it to build the URLs for each symptoms list's page)
# The aforementioned URLs have this kind of format '/sintomi_a_z.php?lettera=' so we need to put the main URL in front
main_site_url = "http://www.my-personaltrainer.it"

# Starting page to scrape
page_url = "http://www.my-personaltrainer.it/sintomi_a_z.php"

# Name of the CSV file that contains the data
csv_file_name = 'index.csv'

# Path of the file to check
csv_file_path = './index.csv'

# Columns of the CSV
csv_columns = ['id', 'symptoms_names']

# Get the parsed page to scrape
soup = getOpenParsedPage(page_url)

# Get the 'div' with the index containing a link to each symptoms list's page
index_div = soup.find("div", attrs={"id": "indice"})
# Get the 'a' elements with a href attached to them
index_a_sympt = index_div.find_all("a", href=True)

# Will contain all the symptoms
symptoms = []

if(checkIfFileExists(csv_file_path)):
    print("File already exists")
else:
    createNewCSV(csv_file_name, csv_columns)

# The id of the symptom, it'll be incremented in the for
id = 0

for a in index_a_sympt:
    # Open and parse the new page
    page_letter_url = main_site_url + a['href']
    print(page_letter_url)
    soup_letter = getOpenParsedPage(page_letter_url)
    # Get the 'div' with id 'resulati' and get each 'a' inside of it (beware we need the href)
    # PS: It's spelled wrong so beware
    results_a = soup_letter.find("div", attrs={"id": "risulati"})
    for a_l in results_a:
        if (a_l.string != "\n" and a_l.string != "Nessun sintomo con questa lettera."):
            id = id + 1
            symptoms.append([str(id), a_l.string])

    print("Wait 20 sec")
    time.sleep(3)

# Update the file
for i in range(len(symptoms)):
    updateCSVRow(csv_file_name, symptoms[i])

print("FINISH")

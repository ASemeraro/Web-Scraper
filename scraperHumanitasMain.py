'''
####################################
# Author: Semeraro Alessandra      #
####################################
'''


from urllib.request import urlopen
from bs4 import BeautifulSoup
import time
import numpy as np

'''
********************
FUNCTIONS DEFINITION
********************
'''
def getOpenParsedPage(url):
    """
    This function opens the page whose URL has been passed as a param; then the page is parsed in a bs4 format to be
    manipulated.
    :param url: a string with the URL of the page to scrape
    :return: a BeautifulSoup object that can be manipulated to access all the parts of the HTML
    """
    # Open page to scrape
    page = urlopen(url)
    # Parse the page in a bs4 format
    soup = BeautifulSoup(page, "html.parser")
    return soup

def getNode(parent, tag, attributes, mode=''):
    """
    This function gets the node/nodes specified by the param.
    :param parent: The parent node
    :param tag: The HTML tag to target
    :param attributes: The attributes of one specific node (like id, class, ...)
    :param mode: Whether we want to find a single node or multiple ones
    :return: The node/nodes found
    """
    if(mode == 'SINGLE'):
        node = parent.find(tag, attrs=attributes)
        return node
    elif(mode == 'ALL'):
        node = parent.find_all(tag, attrs=attributes)
        return node

def setArraySymptomsFrom(array_of_li):
    """
    This function sets the global list symptoms.
    :param array_of_li: A list of 'li' elements from which the fun extracts the 'a' element
    """
    for li2 in array_of_li:
        a_exams_list = li2.find("a", href=True)
        if (a_exams_list.string != "\n"):
            symptoms.append([a_exams_list.string])

def setSymptomsFromPage(page_url):
    """
    This function opens a new page whose list of symptoms must be extracted. After the list is available, there is a
    call to the fun setArraySymptomsFrom(list). This fun also checks if there is another page in the pagination tag,
    if so it will return the link to that page.
    :param page_url: The URL of the page to open and scrape
    :return: a string with the link to the next page (if it exists), None if there is no next page
    """
    print(page_url)

    # Open and parse the new page
    soup_page = getOpenParsedPage(page_url)
    # Get the 'ul' with class 'exams-list', get each 'li' inside of it and each 'a' inside of each 'li'
    ul_exams_list = getNode(soup_page, "ul", {"class": "exams-list"}, mode='SINGLE')
    li_ul_exams_list = getNode(ul_exams_list, "li", {}, mode='ALL')
    setArraySymptomsFrom(li_ul_exams_list)
    # Get the 'li' with class 'next'. It has the link to next page otherwise the class doesn't exists at all
    next = soup_page.find("li", attrs={"class":"next"})
    a_next = next.find("a", href=True)

    if(a_next != None):
        return a_next['href']
    else:
        return None

'''
********************
      SCRAPER
********************
'''

# List of symptoms
symptoms = []

# Starting page to scrape
page_url = "http://www.humanitas.it/sintomi?letter=A"
# Get the parsed page to scrape
soup = getOpenParsedPage(page_url)
# Get the 'div' with the index containing a link to each symptoms list's page
div_alphabet = getNode(soup, "div", {"class": "alphabet"}, mode='SINGLE')
# Get the 'ul' element
ul_div_alphabet = getNode(div_alphabet, "ul", {}, mode='SINGLE')
# Get the 'li' elements - ARRAY
li_ul_div_alphabet = getNode(ul_div_alphabet, "li", {}, mode='ALL')

for li in li_ul_div_alphabet:
    a_li = li.find("a", href=True)
    next = setSymptomsFromPage(a_li['href'])

    while (next != None):
        print("Wait 5 sec")
        time.sleep(5)
        next = setSymptomsFromPage(next)

    print("Wait 5 sec")
    time.sleep(5)
    print(symptoms)

# Save list to a file so that the scraper is run just once
print("Saving the list...")
np.save("list_symptoms", symptoms)



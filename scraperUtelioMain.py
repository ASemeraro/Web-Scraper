'''
####################################
# Author: Semeraro Alessandra      #
####################################
'''


from urllib.request import urlopen
from bs4 import BeautifulSoup
import time
import numpy as np

'''
********************
FUNCTIONS DEFINITION
********************
'''
def getOpenParsedPage(url):
    """
    This function opens the page whose URL has been passed as a param; then the page is parsed in a bs4 format to be
    manipulated.
    :param url: a string with the URL of the page to scrape
    :return: a BeautifulSoup object that can be manipulated to access all the parts of the HTML
    """
    # Open page to scrape
    page = urlopen(url)
    # Parse the page in a bs4 format
    soup = BeautifulSoup(page, "html.parser")
    return soup

def getNode(parent, tag, attributes, mode=''):
    """
    This function gets the node/nodes specified by the param.
    :param parent: The parent node
    :param tag: The HTML tag to target
    :param attributes: The attributes of one specific node (like id, class, ...)
    :param mode: Whether we want to find a single node or multiple ones
    :return: The node/nodes found
    """
    if(mode == 'SINGLE'):
        node = parent.find(tag, attrs=attributes)
        return node
    elif(mode == 'ALL'):
        node = parent.find_all(tag, attrs=attributes)
        return node

def setListBodyPartsFrom(list_of_td):
    """
    This function sets the global list body_parts.
    :param list_of_td: A list of 'td' elements from which the fun extracts the text
    """
    for td in list_of_td:
        body_parts.append(td.string)

'''
********************
      SCRAPER
********************
'''

# List of body parts
body_parts = []

# Starting page to scrape
page_url = "https://www.utelio.it/parti-del-corpo-in-inglese.php"
# Get the parsed page to scrape
soup = getOpenParsedPage(page_url)
# Get the 'table' where each cell contains a body part
table_duen = getNode(soup, "table", {"class": "duen"}, mode='SINGLE')
# Get the 'tr' elements
tr_table_duen = getNode(table_duen, "tr", {}, mode='ALL')

for tr in tr_table_duen:
    # Beware, the class is bl (BL) not b1 (b one)
    td = getNode(tr, "td", {"class" : "bl"}, mode='ALL')
    setListBodyPartsFrom(td)

# Save list to a file so that the scraper is run just once
print("Saving the list...")
np.save("list_body_parts", body_parts)


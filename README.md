<h1>README</h1>
This project is meant to be used in conjunction with my graduation final project. It involves three **web scrapers**:
1. [MyPersonalTrainer](http://www.my-personaltrainer.it/sintomi_a_z.php?lettera=a) Web Scraper;
1. [Humanitas](http://www.humanitas.it/sintomi?letter=A) Web Scraper;
1. [Utelio](https://www.utelio.it/parti-del-corpo-in-inglese.php) Web Scraper.

It's pretty evident, from the links themselves, how the focus of the scraping was basically getting a comprehensive list of medical related information.

<h2>DISCARDED</h2>
The scraper for MyPersonalTrainer works, but I noticed how some symptoms are inadequate for my project's purposes. This is why I decided to not refine the code of said scraper in favour of providing reusability for the second one.

<h2>PREREQUISITES</h2>
1. You must, of course, have [Python](https://www.python.org/) installed (I used version 3.5);
1. Then you must have [Anaconda](https://www.youtube.com/watch?v=YJC6ldI3hWk) (Here is a useful video that shows how to install it).

<h2>HOW TO USE</h2>
1. First run the **scraperHumanitasMain.py** module, what you'll get is a file with a list of symptoms;
1. Second run the **scraperUtelioMain.py** module, what you'll get is a file with a list of body parts;
1. Then run the **choiceOfOutput.py** module, this will prompt the user with a multiple choice menu with formatting/saving preferences.

The possible choices are:

**Number** | **Output Format**
---------- | -----------------
1 | Standard CSV with 4 columns (id, name, synonym, b_flag)
2 | Standard JSON with 3 attributes per symptoms (name, synonym, b_flag)
3 | (SYMPTOMS) Special CSV with 3 columns (name, name, synonym) for DialogFlow
4 | (BODY) Special CSV with 3 columns (name, name, synonym) for DialogFlow (needs manual adjustment with "right/left" values)
0 | EXIT

<h2>DESCRIBING THE DATA</h2>
<h3>SYMPTOMS</h3>
1. **id**: auto increment id with leading zeros (e.g. 020);
1. **name**: name of the symptom;
1. **synonym**: a synonym for the symtom;
1. **b_flag**: a flag that will be used to handle special cases in the final project's logic, do not concern yourselves with it.

<h2>JSON EXAMPLE</h2>
<h3>SYMPTOMS</h3>
Here is an example of how the JSON looks like:

```json
{
  "298": {
    "needs_body_flag": false,
    "synonym": "",
    "name": "Stridore"
  },
  "093": {
    "needs_body_flag": false,
    "synonym": "",
    "name": "Dolore articolare"
  },
  ...
}
```

Here is an example of how the JSON (with key=name) looks like:

```json
{
  "Secchezza nasale": {
    "needs_body_flag": false,
    "name": "Secchezza nasale",
    "synonym": ""
  },
  "Brividi": {
    "needs_body_flag": false,
    "name": "Brividi",
    "synonym": ""
  },
  ...
}
```
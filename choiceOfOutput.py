'''
####################################
# Author: Semeraro Alessandra      #
####################################
'''


import os
import re
import numpy as np
import pandas as pd
import json
import csv

'''
********************
  CLEANING AND CSV
********************
'''

def checkIfFileExists(file):
    """
    This function checks if a file in the project directory exists or not.
    :param file: a string with the path to the file
    :return: True if the file exists, False if the file doesn't exist
    """
    return os.path.exists(file)

def createNewCSV(csv_name, data_frame_to_save, csv_path):
    """
    This function creates a new CSV file with the columns specified by the param columns_name.
    :param csv_name: a string with the name of the CSV to create
    :param data_frame_to_save: a DataFrame with the appropriately formatted data
    :param csv_path: to check if the file exists
    """
    if(checkIfFileExists(csv_path)):
        os.remove(csv_path)
        data_frame_to_save.to_csv(csv_name, index=False, encoding='utf-8', quoting=csv.QUOTE_ALL)
        print("Exit to save!")

    else:
        data_frame_to_save.to_csv(csv_name, index=False, encoding='utf-8', quoting=csv.QUOTE_ALL)
        print("Exit to save!")

def createNewJSON(json_name, dict_to_save, json_path):
    """
    This function creates a new JSON file with the dictionary passed a param.
    :param json_name: a string with the name of the JSON to create
    :param dict_to_save: a dictionary with the data
    :param json_path: to check if the file exists
    """
    if(checkIfFileExists(json_path)):
        os.remove(json_path)
        with open(json_name, 'w') as outfile:
            json.dump(dict_to_save, outfile, indent=2)
        print("Exit to save!")

    else:
        with open(json_name, 'w') as outfile:
            json.dump(dict_to_save, outfile, indent=2)
        print("Exit to save!")

def getSymEntityDataToSave():
    '''
    This function formats the data in a format that can be read by DialogFlow and following this structure
    (name, name, synonym)
    :return: symptoms_to_save: The formatted list with the data
    '''

    # List that will contain the final data
    symptoms_to_save = []

    # Now we need to search for synonyms, they are currently enclosed in round brackets (like so)
    # So we separate the synonym from the name itself
    for i, s in enumerate(symptoms):
        # We are going to add some informations to the symptoms (name, synonym)
        s_name = s[0]
        s_synonym = ''

        # Let's get rid of the quotes
        if (s_name[0] == '"' and s_name[-1] == '"'):
            s_name = s_name.replace('"', '')

        # Let's search for the synonym in round brackets
        p = re.compile(r'\(.*?\)')
        matches = p.finditer(s_name)
        for match in matches:
            s_synonym = match.group(0)
            s_name = (s_name.replace(s_synonym, '')).strip()
            s_synonym = s_synonym[1: -1]

        # Let's populate the new list symptoms_to_save with the new element
        s[0] = s_name
        s = np.insert(s, 1, s_name)
        s = np.insert(s, 2, s_synonym)
        symptoms_to_save.append(s)

    return symptoms_to_save

def getSymStdDataToSave(with_id=True):
    '''
    This function formats all data as specified by the param with_id= and following this structure
    (id(fac), name, synonym, needs_body_flag)
    :param with_id: A flag that determines whether each element of the list will have an id or not
    :return: symptoms_to_save: The formatted list with the data
    '''
    # List that will contain the final data
    symptoms_to_save = []

    # Now we need to search for synonyms, they are currently enclosed in round brackets (like so)
    # So we separate the synonym from the name itself
    for i, s in enumerate(symptoms):
        # We are going to add some informations to the symptoms (id, name, synonym, needs_body_flag)
        s_name = s[0]
        s_synonym = ""
        b_flag = False

        if (s_name[0] == '"' and s_name[-1] == '"'):
            s_name = s_name.replace('"', '')

        p = re.compile(r'\(.*?\)')
        matches = p.finditer(s_name)
        for match in matches:
            s_synonym = match.group(0)
            s_name = (s_name.replace(s_synonym, '')).strip()
            s_synonym = s_synonym[1: -1]

        # Let's populate the list symptoms_to_save with the new element
        # If the flag is set to True then insert an id
        if(with_id == True):
            s = np.insert(s, 0, str(i).zfill(3))
            s[1] = s_name
            s = np.insert(s, 2, s_synonym)
            s = np.insert(s, 3, '') # Add a space for the flag
        else:
            s[0] = s_name
            s = np.insert(s, 1, s_synonym)
            s = np.insert(s, 2, '') # Add a space for the flag

        s = s.tolist() # Change to list because the numpy array doesn't support booleans
        s[-1] = b_flag

        symptoms_to_save.append(s)

    return symptoms_to_save

def getBodyEntityDataToSave():
    '''
    This function formats the data in a format that can be read by DialogFlow and following this structure
    (name, name, synonym)
    :return: body_parts_to_save: The formatted list with the data
    '''

    # List that will contain the final data
    body_parts_to_save = []

    for i, s in enumerate(body_parts):
        # We are going to add some information to the body parts (name, synonym)
        row_entry = []
        s_name = s
        s_synonym = ''

        # Let's get rid of the quotes
        if (s_name[0] == '"' and s_name[-1] == '"'):
            s_name = s_name.replace('"', '')

        # Let's populate the new list symptoms_to_save with the new element
        row_entry.append(s_name)
        row_entry.append(s_name)
        row_entry.append(s_synonym)
        body_parts_to_save.append(row_entry)

    return body_parts_to_save

def saveDataToCSV(data_to_save, csv_file_name, csv_columns, csv_file_path):
    '''
    This function creates a DataFrame with the data and calls the "save" function.
    :param data_to_save: List of data to be converted in a DataFrame
    :param csv_file_name: Name of the CSV file to be created
    :param csv_columns: Columns of the CSV file to be created
    :param csv_file_path: Path of the CSV file, to check whether it exists or not
    '''
    df = pd.DataFrame(data_to_save, columns=csv_columns)
    createNewCSV(csv_file_name, df, csv_file_path)

def saveSymStdDataToJSON(symptoms_to_save, json_name, json_attr, json_path):
    '''
    This function creates a dictionary with the data and calls the "save" function.
    :param symptoms_to_save: List of data to be converted in a DataFrame
    :param json_name: Name of the JSON file to be created
    :param json_attr: Attributes of the JSON file to be created
    :param json_path: Path of the JSON file, to check whether it exists or not
    '''
    # The final dictionary to save
    dic_final = {}
    for i, s in enumerate(symptoms_to_save):
        # Add the attributes to each value in the list
        new_dic = dict(zip(json_attr, s))

        # Add a key (auto increment id) to the symptom s in the list symptoms_to_save
        # dic_final[str(i).zfill(3)] = new_dic

        # Add a key with the name of the symptom to make the fetching part easier
        dic_final[s[0]] = new_dic

    createNewJSON(json_name, dic_final, json_path)

def setUI():
    print("Welcome to myScraper")
    print("Choose one of these options (typing the respective number):")
    print("1. Save symptoms data to CSV")
    print("2. Save symptoms data to JSON")
    print("3. (Symptoms ver) Save just what's needed for the entity in DialogFlow to CSV")
    print("4. (Body parts ver) Save just what's needed for the entity in DialogFlow to CSV")
    print("0. EXIT")
    print()
    choice = input("Write your choice HERE: ")

    while (choice != "0"):
        if (choice == "1"):
            # Name of the CSV file that contains the data
            csv_file_name = 'std_csv.csv'

            # Path of the file to check
            csv_file_path = './std_csv.csv'

            # Columns of the CSV
            csv_columns = ['id', 'name', 'synonym', 'needs_body_flag']

            # save to CSV
            saveDataToCSV(getSymStdDataToSave(with_id=True), csv_file_name, csv_columns, csv_file_path)

        elif (choice == "2"):
            # Name of the CSV file that contains the data
            json_name = 'std_json.json'

            # Path of the file to check
            json_path = './std_json.json'

            # Columns of the CSV
            json_attr = ['name', 'synonym', 'needs_body_flag']

            # save to JSON
            saveSymStdDataToJSON(getSymStdDataToSave(with_id=False), json_name, json_attr, json_path)

        elif (choice == "3"):

            # Name of the CSV file that contains the data
            csv_file_name = 'sym_entity_csv.csv'

            # Path of the file to check
            csv_file_path = './sym_entity_csv.csv'

            # Columns of the CSV
            csv_columns = ['ref', 'name', 'synonym']

            # save entity to CSV
            saveDataToCSV(getSymEntityDataToSave(), csv_file_name, csv_columns, csv_file_path)
        elif (choice == "4"):
            # Name of the CSV file that contains the data
            csv_file_name = 'body_entity_csv.csv'

            # Path of the file to check
            csv_file_path = './body_entity_csv.csv'

            # Columns of the CSV
            csv_columns = ['ref', 'name', 'synonym']

            # save entity to CSV
            saveDataToCSV(getBodyEntityDataToSave(), csv_file_name, csv_columns, csv_file_path)
        elif (choice == "0"):
            break
        else:
            print("Wrong input, please write a valid number")

        choice = input("Write your choice HERE: ")

    print("Bye Bye")


'''
********************
        CODE
********************
'''

# The file has been created by the scraper
symptoms = np.load("list_symptoms.npy")
body_parts = np.load("list_body_parts.npy")

setUI()



